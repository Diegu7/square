module square(
	input clk,
	input rst,
	output reg [2:0] vga_r,
	output reg [2:0] vga_g,
	output reg [1:0] vga_b,
	output reg vga_hs,
	output reg vga_vs
);

wire vga_clk;

// clk100mhz es la señal del clock de entrada a 100MHZ
DCM_SP #(.CLKFX_DIVIDE(8), .CLKFX_MULTIPLY(2), .CLKIN_PERIOD(10))

vga_clock_dcm (.CLKIN(clk), .CLKFX(vga_clk), .CLKFB(0), .PSEN(0), .RST(0));

//Registros que manejan el pincel
reg [9:0] c_row;
reg [9:0] c_col;
reg [9:0] c_hor;
reg [9:0] c_ver;

//Registros que manejan el estado
reg [2:0] cs;
reg [2:0] nxs;

//Registros que manejan la posicion actual del cubo
reg	[9:0] sq_pos_x, sq_pos_x_p;
reg	[9:0] sq_pos_y, sq_pos_y_p;

//Wires que contienen los cuatro borders del cuadrado
wire [9:0] l_sq_pos_x;
wire [9:0] r_sq_pos_x;
wire [9:0] u_sq_pos_y;
wire [9:0] d_sq_pos_y;

//Asignacion de los wires, restados o sumados por el tamano del cuadrado
assign l_sq_pos_x 	= sq_pos_x - 10'd10;
assign r_sq_pos_x 	= sq_pos_x + 10'd10;
assign u_sq_pos_y 	= sq_pos_y - 10'd10;
assign d_sq_pos_y = sq_pos_y + 10'd10;

//Eligiendo el proximo estado
always@(*)begin
	nxs = 3'h0;
	case(cs)
		3'h0: begin
			nxs = 3'h1;
		end
		3'h1: begin
			if(sq_pos_x < 10'd580)
				nxs = 3'h1;
			else
				nxs = 3'h2;
		end
		3'h2: begin
			if(sq_pos_y < 10'd440)
				nxs = 3'h2;
			else
				nxs = 3'h3;
		end
		3'h3: begin
			if(sq_pos_x > 10'd60)
				nxs = 3'h3;
			else
				nxs = 3'h4;
		end
		3'h4: begin
			if(sq_pos_y > 10'd40)
				nxs = 3'h4;
			else
				nxs = 3'h1;
		end
	endcase
end

//Eligiendo la proxima posicion del cuadrado
always@(cs)begin
	case(cs)
		3'h0: {sq_pos_x_p, sq_pos_y_p} = {sq_pos_x, sq_pos_y};
		3'h1: {sq_pos_x_p, sq_pos_y_p} = {sq_pos_x + 10'd2, sq_pos_y};
		3'h2: {sq_pos_x_p, sq_pos_y_p} = {sq_pos_x, sq_pos_y + 10'd3};
		3'h3: {sq_pos_x_p, sq_pos_y_p} = {sq_pos_x - 10'd4, sq_pos_y};
		3'h4: {sq_pos_x_p, sq_pos_y_p} = {sq_pos_x, sq_pos_y - 10'd5};
	endcase
end

//Protocolo de vga + eligiendo el color de la pantalla en base al estado actual
always @ (posedge vga_clk) begin
	//Cuando se presiona el reset todos los registros regresan a su estado inicial
	if(!rst) begin
		c_hor <= 0;
		c_ver <= 0;
		vga_hs <= 1;
		vga_vs <= 0;
		c_row <= 0;
		c_col <= 0;
		sq_pos_x <= 16'd60;
		sq_pos_y <= 16'd60;
		cs <= 0;
	end
	else begin
		if(c_hor < 10'd800 - 1) begin
			c_hor <= c_hor + 10'd1;
		end
		else begin
			c_hor <= 0;
			if(c_ver < 10'd525 - 1) begin
				c_ver <= c_ver + 10'd1;
			end
			else begin
				//Se acutaliza el siguiente estado y la posicion del cuadrado cada vez que termina de pintar un frame entero
				sq_pos_x <= sq_pos_x_p;
				sq_pos_y <= sq_pos_y_p;
				cs <= nxs;
				c_ver <= 0;
			end
		end
	end
	if(c_hor < 10'd640 + 10'd16 + 1 || c_hor > 10'd640 + 10'd16 + 10'd96) begin
		vga_hs <= ~1'b0;
	end
	else begin
		vga_hs <= 1'b0;
	end
	if(c_ver < 10'd480 + 10'd10 || c_ver > 10'd480 + 10'd10 + 10'd2) begin
		vga_vs <= ~1'b1;
	end
	else begin
		vga_vs <= 1'b1;
	end

	if(c_hor < 10'd640) begin
		c_col <= c_hor;
	end
	if(c_ver < 10'd480) begin
		c_row <= c_ver;
	end
	//Pintando la pantalla ~!
	if(c_hor < 10'd640 && c_ver < 10'd480)begin
		if(c_row == 0 || c_col == 0 || c_row == 10'd480-1 || c_col == 10'd640-1) begin
			vga_r <= 7;
			vga_g <= 0;
			vga_b <= 0;
		end
		else if(c_col > l_sq_pos_x && c_col < r_sq_pos_x && c_row > u_sq_pos_y && c_row < d_sq_pos_y) begin
			case(cs)
				3'h1: begin
					vga_r <= 5;
					vga_g <= 0;
					vga_b <= 3;
				end
				3'h2: begin
					vga_r <= 0;
					vga_g <= 0;
					vga_b <= 0;
				end
				3'h3: begin
					vga_r <= 0;
					vga_g <= 5;
					vga_b <= 3;
				end
				3'h4: begin
					vga_r <= 0;
					vga_g <= 7;
					vga_b <= 0;
				end
				default: begin
					vga_r <= 5;
					vga_g <= 0;
					vga_b <= 3;
				end
			endcase
			
		end
		else begin
			case(cs)
				3'h1: begin
					vga_r <= 7;
					vga_g <= 1;
					vga_b <= 1;
				end
				3'h2: begin
					vga_r <= 7;
					vga_g <= 7;
					vga_b <= 3;
				end
				3'h3: begin
					vga_r <= 5;
					vga_g <= 0;
					vga_b <= 3;
				end
				3'h4: begin
					vga_r <= 1;
					vga_g <= 1;
					vga_b <= 3;
				end
				default: begin
					vga_r <= 7;
					vga_g <= 1;
					vga_b <= 1;
				end
			endcase
		end
	end
	else begin
		vga_r <= 0;
		vga_g <= 0;
		vga_b <= 0;
	end
end

endmodule